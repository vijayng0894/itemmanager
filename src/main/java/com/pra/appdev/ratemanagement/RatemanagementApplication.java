package com.pra.appdev.ratemanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RatemanagementApplication {

	public static void main(String[] args) {
		SpringApplication.run(RatemanagementApplication.class, args);
	}

}
