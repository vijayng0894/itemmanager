package com.pra.appdev.ratemanagement.repository;

import java.util.ArrayList;
import java.util.List;

import javax.management.RuntimeErrorException;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.pra.appdev.ratemanagement.entity.Rate;

@Repository
@Transactional
public class RateRepository {
	@Autowired
	EntityManager entityManager;
	
	public void addRate(Rate rate) {
		entityManager.persist(rate);
	}

	public Rate getRate(Long id) {
		return entityManager.find(Rate.class, id);
	}

	public void deleteRate(Long id) {
		Rate rate = getRate(id);
		if (rate != null) {
			entityManager.remove(rate);
		}
	}

	public void updateRate(Rate rate) {
		entityManager.merge(rate);
	}

}
