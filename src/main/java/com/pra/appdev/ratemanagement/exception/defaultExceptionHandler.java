package com.pra.appdev.ratemanagement.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class defaultExceptionHandler extends ResponseEntityExceptionHandler{
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<Object> handleException(RuntimeException ex,WebRequest req){
		
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).
				body("Internal server error. Please contact admin");
				
	}

}
