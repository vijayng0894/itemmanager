package com.pra.appdev.ratemanagement.entity;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.NotFound;



/*- Design a table "RATE" with fields in MySQL
- RateId <Long>, RateDescription <String>,  - Design a table "RATE" with fields in MySQL
                - RateId <Long>, RateDescription <String>, RateEffectiveDate <Date>, RateExpirationDate <Date>, Amount <Integer> 
                - RateId is the primary key
                - All are Not Null fields expect RateDescription
 <Date>, RateExpirationDate <Date>, Amount <Integer> 
- RateId is the primary key
- All are Not Null fields expect RateDescription
*/
@Entity
@Table(name="Rate")
public class Rate {
    @Id
	private Long rateId;
    
    private String rateDescription;
    
    @NotNull(message="rateEffectiveDate Required")
    private LocalDate rateEffectiveDate;
    
    @NotNull(message="rateExpirationDate Required")
    private LocalDate rateExpirationDate;
    
    @NotNull(message="amount Required")
    private Integer amount;
    
    @Transient
    private String surcharge;

	public String getSurcharge() {
		return surcharge;
	}

	public void setSurcharge(String surcharge) {
		this.surcharge = surcharge;
	}

	public Long getRateId() {
		return rateId;
	}

	public void setRateId(Long rateId) {
		this.rateId = rateId;
	}

	public String getRateDescription() {
		return rateDescription;
	}

	public void setRateDescription(String rateDescription) {
		this.rateDescription = rateDescription;
	}

	public LocalDate getRateEffectiveDate() {
		return rateEffectiveDate;
	}

	public void setRateEffectiveDate(LocalDate rateEffectiveDate) {
		this.rateEffectiveDate = rateEffectiveDate;
	}

	public LocalDate getRateExpirationDate() {
		return rateExpirationDate;
	}

	public void setRateExpirationDate(LocalDate rateExpirationDate) {
		this.rateExpirationDate = rateExpirationDate;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Rate(Long rateId, String rateDescription, LocalDate rateEffectiveDate, LocalDate rateExpirationDate,
			Integer amount) {
		super();
		this.rateId = rateId;
		this.rateDescription = rateDescription;
		this.rateEffectiveDate = rateEffectiveDate;
		this.rateExpirationDate = rateExpirationDate;
		this.amount = amount;
	}

	public Rate() {
		super();
	}
    
}
