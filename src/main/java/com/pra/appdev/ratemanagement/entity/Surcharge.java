package com.pra.appdev.ratemanagement.entity;

public class Surcharge {

	private String status;

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Surcharge(String status) {
		super();
		this.status = status;
	}
	
	public Surcharge() {
	
	
	}
	
}
