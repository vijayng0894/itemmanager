package com.pra.appdev.ratemanagement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.pra.appdev.ratemanagement.entity.Rate;
import com.pra.appdev.ratemanagement.entity.Surcharge;
import com.pra.appdev.ratemanagement.repository.RateRepository;

@RestController
public class RateController {
	@Autowired
	RateRepository repo;

	// get all Rate
	@GetMapping("/getRate/{rateId}")
	public ResponseEntity getRate(@PathVariable Long rateId) {
		Rate rate = repo.getRate(rateId);
		if (null != rate) {
			return ResponseEntity.status(HttpStatus.OK).body(rate);

		}

		else {
			return (ResponseEntity) ResponseEntity.status(HttpStatus.NOT_FOUND).body("rateId not found in RMS");
		}

	}

	// Add Rate
	@PostMapping("ratemanagement/addRate")
	public String postRate(@RequestBody Rate rate) {
		repo.addRate(rate);
		return "success";
	}

	// delete Rate
	@DeleteMapping("/ratemanagement/deleterate/{rateId}")
	public ResponseEntity deletemyRate(@PathVariable Long rateId) {
		
		ResponseEntity responseEntity=getRate(rateId);
		if(responseEntity.getStatusCode().equals(HttpStatus.OK))
		{
			repo.deleteRate(rateId);
			return (ResponseEntity) ResponseEntity.status(HttpStatus.OK).body("Data deleted successfully");
		}
		else {
			return responseEntity;
		}
	
	}

	// update Rate
	@PutMapping("ratemanagement/updaterate")
	public String updateMyRate(@RequestBody Rate rate) {
		repo.updateRate(rate);
		return "update successfull";
	}

	// get with surcharge
	@GetMapping("ratemanagement/search-rate/{rateId}")
	public Rate fetchRateSurcharge(@PathVariable Long rateId) {
		Rate rate = null;
		ResponseEntity<Surcharge> responseEntity = new RestTemplate()
				.getForEntity("https://surcharge.free.beeceptor.com/surcharge", Surcharge.class);
		Surcharge obj1 = responseEntity.getBody();
		if (obj1 != null) {
			rate = repo.getRate(rateId);
			rate.setSurcharge(obj1.getStatus());
		}

		return rate;
	}

}
